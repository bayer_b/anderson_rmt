#include <iostream>
#include "dataset.h"

#include <fstream>
#include <sstream>

void dataset::Dataset::generate()
{
    for (size_t row = 0; row < M_size; row++)
    {
        for (size_t column = 0; column <= row; column++)
        {
            M(row, column) = random_generator::rand_var();
        }
    }
}

void dataset::Dataset::computeEigenvalues()
{
    es.compute(M);
    v = es.eigenvalues();
}

void dataset::Dataset::printResultsToFile(const char *fileNameStartsWith)
{

    std::stringstream fileName;
    fileName << fileNameStartsWith << "_" << generationId;

    std::ofstream file(fileName.str());

    file << "{\"generationId\":" << generationId << ",";

    file << "\"values\":[";

    for (int i = 0; i != (M_size - 1); i++)
        file << v[i] << ",";
    file << v[M_size-1];

    file << "]}";

    file.close();

}
