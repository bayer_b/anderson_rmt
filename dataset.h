#ifndef DATASET_H
#define DATASET_H

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

#include "random_generator.h"

namespace dataset {

class Dataset
{

public:

    Dataset(int generationId_=0, size_t size=0): generationId(generationId_), M(size, size), M_size(size) {}
    ~Dataset() {};

    const Eigen::MatrixXf& get_M() {return this->M;}
    const Eigen::SelfAdjointEigenSolver<Eigen::MatrixXf>::RealVectorType& get_v() {return this->v;}

    void setGenerationId(int id) { generationId = id; }
    void generate();
    void computeEigenvalues();
    void printResultsToFile(const char *);

private:

    Dataset(const Dataset &other) {}
    Dataset(Dataset &&other) noexcept {}
    Dataset &operator=(const Dataset &other) {return *this;}
    Dataset &operator=(const Dataset &&other) noexcept {return *this;}

    int generationId;

    Eigen::MatrixXf M;
    size_t M_size;
    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXf> es;
    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXf>::RealVectorType v;


};

}

#endif // DATASET_H
