#ifndef RANDOM_GENERATOR_H
#define RANDOM_GENERATOR_H

#include <random>

namespace random_generator {

static std::default_random_engine generator;
static std::uniform_real_distribution<double> distribution(-1.0, 1.0);
static auto rand_var = std::bind (distribution, generator);

}


#endif // RANDOM_GENERATOR_H
