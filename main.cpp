#include <iostream>

#include "dataset.h"

#include "helper.h"

#include <array>

#include <sstream>

int main()
{



    #pragma omp parallel for num_threads(4)
    for (int i = 0; i < 100000; ++i)
    {
        dataset::Dataset X(i, 1000);
        X.generate();
        X.computeEigenvalues();
        X.printResultsToFile("/mnt/hdfs/Anderson_RMT-results/set");
    }

    return 0;
}

