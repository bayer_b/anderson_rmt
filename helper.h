#ifndef HELPER_H
#define HELPER_H

#include <functional>
#include <chrono>

namespace helper {

template<typename ObjT, typename FuncT, typename ...Args>
static auto measure_runtime_memb_func(ObjT obj, FuncT func, Args&&... args)
{
    auto start = std::chrono::steady_clock::now();
    (obj.*func)(std::forward<Args>(args)...);
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-start);
}

struct measure_runtime
{
    template<typename FuncT, typename ...Args>
    static auto duration(FuncT func, Args&&... args)
    {
        auto start = std::chrono::steady_clock::now();
        func(std::forward<Args>(args)...);
        return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-start);
    }
};


}

#endif // HELPER_H
